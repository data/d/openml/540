# OpenML dataset: mu284

https://www.openml.org/d/540

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**:   
**Source**: Unknown - Date unknown  
**Please cite**:   

This file contains the data in "The MU284 Population" from Appendix B of
the book "Model Assisted Survey Sampling" by Sarndal, Swensson and Wretman,
published by Springer-Verlag, New York, 1992. The data set contains 284
observations on 11 variables, plus a line with variabel names. Please
consult the mentioned appendix for more information about this data set.
The data were scanned from the book and interpreted with OCR-technique.
Please note that errors may occur in such a process. The result was
macro-edited against "The Clustered MU284 Population" in Appendix C of the
book. Please use the data at your own risk - I take no responsibility for
any problems eventual remaining errors will cause you.
four typos in the first printing of the book have been corrected:
Label 107, ME84 should be 1100, not 1110
Label 141, RMT85 should be 396, not 369
Label 220, ME84 should be 461, not 491
Label 229, ME84 should be 1239, not 1238.
The data was submitted to StatLib with the permission of Springer-Verlag
(ref: John Kimmel).
Esbjorn Ohlsson
Stockholm University
esbj@matematik.su.se


Information about the dataset
CLASSTYPE: numeric
CLASSINDEX: none specific

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/540) of an [OpenML dataset](https://www.openml.org/d/540). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/540/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/540/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/540/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

